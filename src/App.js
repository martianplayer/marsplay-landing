import React, { Component } from 'react';
import './App.css';
import AppBody from './AppBody';
import AppBodyMobile from './AppBodyMobile';



class App extends Component {
	state = {

	}

	componentDidMount = () => {
		/* global FB */ 
		window.fbAsyncInit = function() {
			window.FB.init({
				appId            : '1615350118509795',
				autoLogAppEvents : true,
				xfbml            : true,
				version          : 'v2.10'
			});
			window.FB.AppEvents.logPageView();
		}
		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	}
	render() {
		return (
			<div className="app">
			<AppBody/> 
			<AppBodyMobile/> 
			</div>
			);
	}
}

export default App;
