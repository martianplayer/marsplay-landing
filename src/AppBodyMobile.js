import React, { Component } from 'react';
import './App.css';
import artboard from './images/artboard.png'
import logo from './images/logo.png'
import cam from './images/cam.png'
import {ModalContainer, ModalDialog} from 'react-modal-dialog'
import mono from './images/monogram.svg'
import config from './BaseConfig'
import axios from 'axios'
import thumbsup from './images/thumbsup.png'
import fbShare from './images/fbshare.png'
import twShare from './images/twshare.png'


const emailRegex = /^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[A-Za-z]+$/
const twitterUrl = encodeURI("https://twitter.com/intent/tweet?text=I’ve just joined @marsplayco. Building the most human way to shop online. Register today to get free photoshoot. http://marsplay.co")

class AppBodyMobile extends Component {
	state = {
   		isShowingModal: false,
      isShowingConfirmModal: false,
      errorText:"",
      intervalDuration:700,
   		tempEmail:"",
   		spots:0,
   		invites:0, 
   		milCount:'417,166,543',
   		intervalId: "",
   		style: {
			position: 'absolute',
    		top:'80.5px',
    		left:'56px',
    		width:'260px'
		}
	}


	handleClick = () => {
    this.setState({isShowingModal: true})
  this.setState({tempEmail:""})
  this.setState({errorText:""})
  }

  handleClose = () => {
    this.setState({isShowingModal: false})
    if (this.state.tempEmail !== "" && this.state.errorText === "") {
      this.setState({isShowingConfirmModal: true})
    }
  }

  handleConfirmClose = () => {
    this.setState({isShowingConfirmModal: false})
    this.setState({tempEmail:""});
  }


  handleEmailChange = (e) => {
    this.setState({'errorText':""})
  	this.setState({tempEmail:e.target.value});
  }

 intervalFun = () => {
      var val = this.state.milCount+"";
      val = parseInt(val.replace(/,/g,""),10); 
   
      val += Math.floor(Math.random()*5);
      this.setState({
        milCount: val.toLocaleString('en-US')
      }); 
      this.setState({
        intervalDuration: 5000
      })  
  }
  componentWillMount = () => {
  	axios.get(config().API_HOST+'/tools/invite').then((response)=> {
		this.setState({spots:response.data.spots});
		this.setState({invites:response.data.invites});
   this.setState({intervalId: setInterval(this.intervalFun,this.state.intervalDuration)})

	}).catch((err)=> {
		console.log(err);
		alert("An error occurred at our end. Please try again later.")
	})
  }

  submitForm = () => {
    var data = {"email":this.state.tempEmail}; 
    console.log(data);
    if (data.email !== "") {
      if (emailRegex.test(data.email)) {
        this.handleClose()
        axios.post(config().API_HOST+'/tools/invite', data).then((response)=> {
        }).catch((err)=> {
          //console.log(err);
          //alert("An error occurred at our end. Please try again later.")
        })
      } else {
        this.setState({errorText:'Please enter a valid email!'})
      }
    } else {
      this.setState({errorText:'Email is needed to register'})
    }
  }


shareOnFb = () => {
  window.FB.ui({
    method: 'share',
    href: 'http://www.marsplay.co',
  }, function(response){});
}
  render() {
    return (
      <div className="app-body-mobile">
      	

      	<div className="mob-header">
      		<img src={logo} alt=""/> 
      	</div>

      	<div className="mob-take-your-style">
			<span>Time to take  </span><br/><span>your true style to</span> 
		</div>


		<div className="mob-mil-count">
			{this.state.milCount}
		</div>

		  <div className="mob-cam-layer-2">
              <div className="cam-layer-1">
                <div className="cam">
                  <img src={cam} alt="" /> 
                </div>
              </div>
            </div>


            <div className="mob-get-early-access">
              <span>Get early access</span>
              <br/>
              <span>with free photoshoot</span>
            </div>


            <div className="mob-request-invite-left pointer" onClick={this.handleClick}>
              <span className="request-free-invite-left">Request free invite</span>
            </div>
            { 
              <div className="modal-box"> {
              this.state.isShowingModal &&
              <ModalContainer onClose={this.handleClose}>
                <ModalDialog onClose={this.handleClose} style={this.state.style}>
                  <div className="mob-modal-logo">
                    <img src={mono} alt=""/> 
                  </div>
                  <p className="mob-be-the-first">Be the first to get invite when we launch</p>
                  <p className="mob-already-in">{this.state.invites} already in</p>
                  <input type="text" className="mob-email-input" placeholder="enter your email" value={this.state.tempEmail} onChange={this.handleEmailChange}/>
                  <div className="error-text">{this.state.errorText}</div>
                  <div className="mob-modal-submit" onClick={this.submitForm}><span className="mob-modal-submit-text">Submit</span></div>

                </ModalDialog>
              </ModalContainer>
          		}</div>

            }

            {

              <div className="modal-box"> {

               this.state.isShowingConfirmModal &&
               <ModalContainer onClose={this.handleConfirmClose}>
                <ModalDialog onClose={this.handleConfirmClose} style={this.state.style}>
                  <div className="mob-modal-logo">
                    <img src={mono} alt=""/> 
                  </div>
                  <p className="mob-be-the-first">Thanks for the support!<br/>You'll hear from us soon :)</p>
                  <br/>
                  <div className="social-share">
                    <div className="tw-share">
                    <a href={twitterUrl}>
                      <img src={twShare} alt=""/>
                    </a>
                    </div>

                    <div className="fb-share" onClick={this.shareOnFb}>
                      <img src={fbShare} alt=""/>
                    </div>
                  </div>
                <div className="mob-modal-submit" onClick={this.handleConfirmClose}><span className="mob-modal-submit-text">Got it <img src={thumbsup} className="thumbsup mob-thumbsup" alt=""/></span></div>
                <br/>
                <br/>
                </ModalDialog>
              </ModalContainer>

              }</div>

            }
            

            <div className="mob-hurry-slots" >
              Hurry! Only {this.state.spots} slots left
            </div>

            <div className="mob-bg-cont">
      		<img src={artboard} alt="" /> 
      	</div>
      </div>
    );
  }
}

export default AppBodyMobile;
