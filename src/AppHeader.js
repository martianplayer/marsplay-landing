import React, { Component } from 'react';
import './App.css';
import logo from './images/logo.png'
class AppHeader extends Component {
  render() {
    return (
      <div className="app-header">
      <div className="app-header-cont">
        	<div className="logo-cont">
        		<img src={logo} />
        	</div>
        	<div className="round-button-top">
        		<span className="request-free-invite">Request free invite</span>
        	</div>
        </div>
      </div>
    );
  }
}

export default AppHeader;
