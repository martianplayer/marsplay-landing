import React, { Component } from 'react';
import './App.css';
import cam from './images/cam.png'
import phone from './images/phone.png'
import zero from './images/cards/0.png'
import one from './images/cards/two.png'
import two from './images/cards/three.png'
import four from './images/cards/4.png'
import five from './images/cards/5.png'
import six from './images/cards/6.jpg'

import face_0 from './images/cards/o0.png'
import face_1 from './images/cards/o1.png'
import face_2 from './images/cards/o2.png'
import face_3 from './images/cards/o3.png'
import face_4 from './images/cards/o4.png'
import face_5 from './images/cards/o5.png'


import fb from './images/fb.png'
import snap from './images/snap.png'
import ig from './images/ig.png'

import thumbsup from './images/thumbsup.png'
import {ModalContainer, ModalDialog} from 'react-modal-dialog'
import mono from './images/monogram.svg'
import logo from './images/logo.png'
import config from './BaseConfig'
import axios from 'axios'

import fbShare from './images/fbshare.png'
import twShare from './images/twshare.png'

const twitterUrl = encodeURI("https://twitter.com/intent/tweet?text=I’ve just joined @marsplayco. Building the most human way to shop online. Register today to get free photoshoot. http://marsplay.co")

const emailRegex = /^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[A-Za-z]+$/

class AppBody extends Component {

  state = {
   isShowingModal: false,
   isShowingConfirmModal:false,
   errorText:"",
   spots:0, 
   invites:0,
   tempEmail:"",
   milCount:'417,166,543',
   intervalId: "",
   intervalDuration: 1600, 
   cards:[
    {
      image:zero,
      face: face_0, 
      name: "Loretta Matthews",
      heading: "The ultimate facial sauna",
      "body":"Do you feel that you will learn enough..."
    },
    {
      image:one,
      face: face_1, 
      name: "Abbie Carpenter",
      heading: "The life is cool",
      "body":"Fun begins when you start..."
    },
    {
      image:two,
      face: face_2, 
      name: "Caren James",
      heading: "Summer Diva",
      "body":"Beat the heat with..."
    },
    {
      image:four,
      face: face_3,
      name: "Gabriella Simpson",
      heading: "Living in Vegas",
      "body":"Party all night with.."
    },
    {
      image:five,
      face: face_4,
      name: "Michelle Kennedy",
      heading: "The sauve version",
      "body":"Capitalise on the reach..."
    },
    {
      image:six,
      face: face_5, 
      name: "Hariette Black",
      heading: "Killing on the road",
      "body":"Beautiful giveaways on..."
    }
   ]
  }




handleClick = () => {
  this.setState({isShowingModal: true})
  this.setState({tempEmail:""})
  this.setState({errorText:""})
}

  handleClose = () => {
    this.setState({isShowingModal: false})
  }

  handleConfirmClose = () => {
    this.setState({isShowingConfirmModal: false})
    this.setState({tempEmail:""});
  }

  intervalFun = () => {
      var val = this.state.milCount+"";
      val = parseInt(val.replace(/,/g,""),10); 
   
      val += Math.floor(Math.random()*5);
      this.setState({
        milCount: val.toLocaleString('en-US')
      }); 
      this.setState({
        intervalDuration: 5000
      })  
  }


handleEmailChange = (e) => {
    this.setState({'errorText':""})
    this.setState({tempEmail:e.target.value});
  }

    componentWillMount = () => {
    axios.get(config().API_HOST+'/tools/invite').then((response)=> {
    this.setState({spots:response.data.spots});
    this.setState({invites:response.data.invites});


   this.setState({intervalId: setInterval(this.intervalFun,this.state.intervalDuration)})

  }).catch((err)=> {
    console.log(err);
    alert("An error occurred at our end. Please try again later.")
  })
  }


shareOnFb = () => {
  window.FB.ui({
    method: 'share',
    href: 'http://www.marsplay.co',
  }, function(response){});
}
  submitForm = () => {
    var data = {"email":this.state.tempEmail}; 
    console.log(data);
    if (data.email !== "") {
      if (emailRegex.test(data.email)) {
        this.handleClose()
        this.setState({isShowingConfirmModal: true})
        axios.post(config().API_HOST+'/tools/invite', data).then((response)=> {
        }).catch((err)=> {
          //console.log(err);
          //alert("An error occurred at our end. Please try again later.")
        })
      } else {
        this.setState({errorText:'Please enter a valid email!'})
      }
    } else {
      this.setState({errorText:'Email is needed to register'})
    }
  }

  render() {

    const Cards = () => {
      return (<div> {this.state.cards.map(s=>{
        return<div className="card" key={Math.floor(Math.random()*10000)}> 
            <img src={s.image} alt=""/> 
            <div className="card-details">
              <div className="card-line">
                <img className="photoPh" src={s.face} alt=""/>  
                <div className="card-name">
                  {s.name}
                </div>
              </div>

              <div className="card-desc">
                <span className="card-heading">{s.heading}</span>
                <br/>
                <span className="card-body">{s.body}</span>
              </div>
            </div>

          </div> 

      })} </div>)
    }
    
    return (
      <div className="app-body-container-web">
       <div className="app-header">
      <div className="app-header-cont">
          <div className="logo-cont">
            <img src={logo} alt=""/>
          </div>
          <div className="pointer round-button-top" onClick={this.handleClick}>
            <span className="request-free-invite">Request free invite</span>
          </div>
        </div>
      </div>
      <div className="app-body">
      	<div className="app-left">
          <div className="app-left-cont">
        		<div className="take-your-style">
        			<span>Time to take  </span><br/><span>your true style to</span> 
        		</div>
        		<div className="mil-count">
        			{this.state.milCount}
        		</div>

        		<div className="millennials">
        			Millennials
        		</div>

            <div className="cam-layer-2">
              <div className="cam-layer-1">
                <div className="cam">
                  <img src={cam} alt="" /> 
                </div>
              </div>
            </div>

            <div className="get-early-access">
              <span>Get early access</span>
              <br/>
              <span>with free photoshoot</span>
            </div>

            <div className="request-invite-left pointer" onClick={this.handleClick}>
              <span className="request-free-invite-left">Request free invite</span>
            </div>
            {
              this.state.isShowingModal &&
              <ModalContainer onClose={this.handleClose}>
                <ModalDialog onClose={this.handleClose}>
                  <div className="modal-logo">
                    <img src={mono} alt=""/> 
                  </div>
                  <p className="be-the-first">Be the first to get invite when we launch</p>
                  <p className="already-in">{this.state.invites} already in</p>
                  <input type="text" className="email-input" placeholder="enter your email" value={this.state.tempEmail} onChange={this.handleEmailChange}/>
                  <div className="error-text">{this.state.errorText}</div>
                  <div className="modal-submit" onClick={this.submitForm}><div className="modal-submit-text">Submit</div></div>

                </ModalDialog>
              </ModalContainer>
            }
            
            {

              <div className="modal-box"> {

               this.state.isShowingConfirmModal &&
               <ModalContainer onClose={this.handleConfirmClose}>
                <ModalDialog onClose={this.handleConfirmClose} style={this.state.style}>
                  <div className="modal-logo">
                    <img src={mono} alt=""/> 
                  </div>
                  <p className="be-the-first">Thanks for the support!<br/>You'll hear from us soon :)</p>
                  <br/>
                  <br/>
                  <div className="social-share">
                    <div className="tw-share">
                    <a href={twitterUrl}>
                      <img src={twShare} alt=""/>
                      </a>
                    </div>

                    <div className="fb-share" onClick={this.shareOnFb}>
                      <img src={fbShare} alt=""/>
                    </div>
                  </div>
                <div className="modal-submit got-it" onClick={this.handleConfirmClose}><div className="modal-submit-text">Got it <img src={thumbsup} className="thumbsup" alt=""/></div></div>
                <br/>
                <br/>
                </ModalDialog>
              </ModalContainer>

              }</div>

            }
            <div className="hurry-slots">
              Hurry! Only {this.state.spots} slots left
            </div>


        	</div>
        </div>



      	<div className="app-right">
          <div className="app-right-cont">
            <div className="phone">
              <img src={phone} alt=""/>
            </div>
            <div className="carousel">
              <div className="carousel-cont">
                <Cards/>
              </div>
            </div>

            <div className="social">
              <a href="https://www.facebook.com/profile.php?id=122576868322085" target="_blank"><img src={fb}alt=""/></a>
              <a href="https://www.snapchat.com/add/marsplay.co" target="_blank"><img src={snap}alt=""/></a>
              <a href="https://www.instagram.com/marsplayco/" target="_blank"><img src={ig}alt=""/></a>
            </div>

          </div> 
      	</div>

        <div className="app-clear"/>
      </div>
      </div>
    );
  }
}

export default AppBody;
